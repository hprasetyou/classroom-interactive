<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-2">
        <a href="#" data-toggle="modal" data-target="#modalQuiz" class="activity-item">
            <div class="activity-item-icon mx-auto">
                <i class="fa fa-file-text-o fa-4x" aria-hidden="true"></i>
            </div>
            <span class="activity-item-text">Quiz</span>
        </a>
    </div>
    <div class="col-sm-2">
        <a href="#" class="activity-item" data-toggle="modal" data-target="#modalTopic">
            <div class="activity-item-icon mx-auto">
                <i class="fa fa-book fa-4x" aria-hidden="true"></i>
            </div>
            <span class="activity-item-text">Topic</span>
        </a>
    </div>
    <div class="col-sm-2">
        <a href="#" class="activity-item" data-toggle="modal" data-target="#modalFile">
            <div class="activity-item-icon mx-auto">
                <i class="fa fa-file fa-4x" aria-hidden="true"></i>
            </div>
            <span class="activity-item-text">File</span>
        </a>
    </div>
</div>
<!--
<div class="row">
    <div class="col-sm">
        <div class="row">
            <div class="offset-sm-3 col-sm-6 ">
                <h6 class="activity-quick text-with-line">Quick Action</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 offset-2">
                <a href="#" class="activity-item">
                    <div class="activity-item-icon activity-item-icon-smaller mx-auto">
                        <span class="icon-text">MC</span>
                    </div>
                    <span class="activity-item-text">Multiple Choice</span>
                </a>
            </div>
            <div class="col-sm-2">
                <a href="#" class="activity-item">
                    <div class="activity-item-icon activity-item-icon-smaller mx-auto">
                        <span class="icon-text">TF</span>
                    </div>
                    <span class="activity-item-text">True/False</span>
                </a>
            </div>
            <div class="col-sm-2">
                <a href="#" class="activity-item">
                    <div class="activity-item-icon activity-item-icon-smaller mx-auto">
                        <span class="icon-text">SA</span>
                    </div>
                    <span class="activity-item-text">Short Answer</span>
                </a>
            </div>
            <div class="col-sm-2">
                <a href="#" class="activity-item">
                    <div class="activity-item-icon activity-item-icon-smaller mx-auto">
                        <span class="icon-text">N</span>
                    </div>
                    <span class="activity-item-text">Notes</span>
                </a>
            </div>
            <div class="col-sm-3"></div>
        </div>
    </div>
</div> -->