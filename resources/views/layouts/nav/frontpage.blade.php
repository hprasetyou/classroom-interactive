<nav class="navbar nav-frontpage navbar-expand-md navbar-light bg-white">
    <div class="container">
        <a class="navbar-brand" href="/">
            Class Interactive
        </a>
        <div class="collapse navbar-collapse">     
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/register">Get Account</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/login/teacher">Teacher Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/login/student">Student Login</a>
                </li>
            </ul> 
        </div>
    </div>
</nav>
